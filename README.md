# Sentiment Analysis on Tweets

**Update**(25 Okt. 2019): This work was done for a Kata.ai Hackathon project and the dataset cannot be released because I don't own the copyright. However, everything in this repository can be easily modified to work with other datasets especially twitter datasets.

## Dataset Information

We use Multi Layer Perceptron (MLP) methods for sentiment analysis on tweets (a binary classification problem). The training dataset is expected to be a csv file of type `tweet_id,sentiment,tweet` where the `tweet_id` is a unique integer identifying the tweet, `sentiment` is either `1` (positive) or `0` (negative), and `tweet` is the tweet enclosed in `""`. Similarly, the test dataset is a csv file of type `tweet_id,tweet`. Please note that csv headers are not expected and should be removed from the training and test datasets.  

## Requirements

There are some general library requirements for the project and some which are specific to individual methods. The general requirements are as follows.  
* `numpy`
* `scikit-learn`
* `scipy`
* `nltk`
* `tensorflow-hub`
* `twitterscraper`
* `textblob`

## Usage

we already explain our step-by-step in  `[Kata_Doi]_Twitter_Sentiment_Analysis.ipynb` file as well. if you have any question, please let us know yusufaisal9@icloud.com